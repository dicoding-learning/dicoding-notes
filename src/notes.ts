type Note = {
	id: string;
	title: string;
	body: string;
	createdAt: string;
	updatedAt: string;
	tags: Array<string>;
};

const notes: Array<Note> = [];

export { notes, Note };
