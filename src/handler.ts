import * as Hapi from "@hapi/hapi";
import { nanoid } from "nanoid";
import { notes, Note } from "./notes";

interface CustomRouteRequest extends Hapi.Request {
	payload: {
		title: string;
		tags: Array<string>;
		body: string;
	};
}

const addNoteHandler = (
	request: CustomRouteRequest,
	h: Hapi.ResponseToolkit
) => {
	const { title, tags, body } = request.payload;
	const id = nanoid();
	const createdAt = new Date().toISOString();
	const updatedAt = createdAt;

	const newNote = { id, title, body, createdAt, updatedAt, tags };
	notes.push(newNote);

	const isSuccess = notes.filter((note: Note) => note.id === id).length > 0;

	if (!isSuccess) {
		const response = h.response({
			status: "fail",
			message: "failed to insert note",
		});
		response.code(500);
		return response;
	}

	const response = h.response({
		status: "success",
		message: "note inserted successfully",
		data: {
			noteId: id,
		},
	});
	response.code(201);
	return response;
};

const getAllNotesHandler = () => ({
	status: "success",
	data: {
		notes,
	},
});

const getNoteByIdHandler = (
	request: CustomRouteRequest,
	h: Hapi.ResponseToolkit
) => {
	const { id } = request.params;
	const note = notes.filter((note: Note) => note.id === id)[0];

	if (note == undefined) {
		const response = h.response({
			status: "fail",
			message: "note not found",
		});
		response.code(404);
		return response;
	}

	return {
		status: "success",
		data: {
			note,
		},
	};
};

const editNoteByIdHandler = (
	request: CustomRouteRequest,
	h: Hapi.ResponseToolkit
) => {
	const { id } = request.params;
	const { title, body, tags } = request.payload;
	const updatedAt = new Date().toISOString();

	const index = notes.findIndex((note: Note) => note.id === id);

	if (index === -1) {
		const response = h.response({
			status: "fail",
			message: "failed to update note",
		});
		response.code(404);
		return response;
	}

	notes[index] = { ...notes[index], title, body, tags, updatedAt };
	const response = h.response({
		status: "success",
		message: "note updated successfully",
	});
	response.code(200);
	return response;
};

const deleteNoteByIdHandler = (
	request: CustomRouteRequest,
	h: Hapi.ResponseToolkit
) => {
	const { id } = request.params;

	const index = notes.findIndex((note: Note) => note.id === id);
	if (index === -1) {
		const response = h.response({
			status: "fail",
			message: "failed to delete note",
		});
		response.code(404);
		return response;
	}

	notes.splice(index, 1);
	const response = h.response({
		status: "success",
		message: "note deleted successfully",
	});
	response.code(200);
	return response;
};

export {
	addNoteHandler,
	getAllNotesHandler,
	getNoteByIdHandler,
	editNoteByIdHandler,
	deleteNoteByIdHandler,
};
